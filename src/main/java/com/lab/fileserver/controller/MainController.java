package com.lab.fileserver.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

@Controller
public class MainController {
	@GetMapping("/")
	public String about() {
		return("about");
	}

	@Value("${file.upload.folder}")
    private String fileUploadFolder;
	
	@PostMapping("/handleFileUpload")
	public String handleFileUpload(@RequestParam("foto") MultipartFile foto,
			RedirectAttributes redirectAttributes) throws IllegalStateException, IOException {
		
		Files.createDirectories(Paths.get(fileUploadFolder));
		
		String absolutePathDest = fileUploadFolder + File.separator + foto.getOriginalFilename();
		
		if (Files.exists(Paths.get(absolutePathDest))) {
			DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH.mm.ss");  
			System.out.println("file sudah ada, melakukan rename file yang sudah ada");
			String originalFilename = foto.getOriginalFilename();
			String backupFilename = originalFilename.substring(0, originalFilename.lastIndexOf(".")) // ambil nama file tanpa extension
				+ "_" + LocalDateTime.now().format(format) // tambah datetimie
				+ originalFilename.substring(originalFilename.lastIndexOf(".")); // terakhir concate dengan extension aslinya
			Files.move(Paths.get(absolutePathDest), Paths.get(fileUploadFolder + File.separator + backupFilename));
		}
		
		Files.write(Paths.get(absolutePathDest), foto.getBytes());
		System.out.println("nama file =====> " + foto.getOriginalFilename());
		System.out.println("absolute path =====> " + absolutePathDest);
		
		return("about");
	}
	
	@Autowired ResourceLoader resourceLoader;
	@Autowired ApplicationContext context;
	
	@GetMapping("/foto/{filename:.+}")
	@ResponseBody
	public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

		
		System.out.println("mencoba mengakses -----> " + fileUploadFolder + File.separator + filename);

		// bisa
//		Resource file = resourceLoader.getResource("file:" + fileUploadFolder + File.separator + filename);
		// bisa
		Resource file = context.getResource("file:" + fileUploadFolder + File.separator + filename);
		
		// tidak bisa 
//		Resource file = new ClassPathResource("file:" + fileUploadFolder + File.separator + filename);
		
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
		"attachment; filename=\"" + file.getFilename() + "\"").body(file);
		
	}
}
